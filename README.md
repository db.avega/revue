# ReVue

## How to initialize

- Install firebase cli https://firebase.google.com/docs/cli
- In a terminal
  - run `firebase projects:create`
  - Choose project id and name
  - Add resources in UI (see screenshots)
  - firebase init

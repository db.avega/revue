import { test, expect } from '@playwright/test';


test("Facit: Logs in and verifies modules load", async ({ page }) => {
  await page.goto("/");

  await page.getByLabel("Din mail").fill("test@test.com");
  await page.getByLabel("Ditt lösenord").fill("Test123");
  await page.getByRole("button", { name: "Skicka" }).click();

  await expect(
    page.getByRole("heading", { name: "Modul 1" }).first()
  ).toBeVisible();
});

// See here how to get started:
// https://playwright.dev/docs/intro
test('User can log in', async ({ page }) => {
  await page.goto('/');
  await expect(page.getByLabel("Din email")).toBeVisible();

  const emailInput = page.getByLabel("Din email").fill("test@test.com")

  const pwInput = page.getByLabel("Ditt lösenord").fill("Test123")

  await page.getByRole("button", { name: "Skicka" }).click();

  await expect(page.getByRole ("button", {name: "Logga ut"})).toBeVisible();
})
